# **KONSEP CLUSTER**

1. Memiliki lebih dari satu node (Multinode)

2. Memiliki satu diantara tiga konsep Mastering:

   - **Master-Worker**

     Cluster memiliki satu node yang digunakan sebagai master yang digunakan untuk query, schedule, dan store-cluster-state. Jika master down, maka cluster tidak bisa berjalan sebagaimana mestinya. `Contoh: Kube.`

   - **Master-master (Masterless)**

     Cluster tidak memiliki satu master khusus, karena semua node berperan sebagai master. Setiap Master algoritma routing untuk berkomunikasi dengan master lainnya, untuk menjalankan layanan. `Contoh: Elasticsearch`

   - **Master-Mon-Worker**

     Cluster memiliki master yang bertugas untuk query dan schedule. Mon bertugas untuk store-cluster-state. Contoh: `Ceph`

3. Memiliki satu diantara dua konsep Avail-Node:

   - Active-active
     Semua node bersifat aktif yang bertugas menjalankan layanan. `Contoh: Message Queueing Service (RabbitMQ, AWS SQS, Apache Kafka)`
   - Active-Passive / Active-Standby / Master-Slave
     Satu node aktif dan sisanya standby. Jika active node mati, maka standby node akan dipromote menjadi master node sehingga cluster dapat avail kembali. `Contoh: Aplikasi database`

# **HIGH-AVAILABILITY CLUSTER**

## **[Failover]**

1. Memiliki VRRP (Virtual Router Redundancy Protocol) untuk assigment VirtualIP ke avail-node. Hanya dapat digunakan terhadap service-service yang menggunakan konsep Active-Passive. `Misal: service di openstack yang diclusterized (multinode per service)`

## **[High-Performance]**

2. Memiliki objek yang berjalan secara simultan atau parallel (Active-active) 

3. Memiliki Health-check. Sebagai continuous check ke setiap objek didalam cluster untuk memastikan bahwa objek tersebut masih available untuk menjalankan layanan (berdasarkan metrics yang berhasil didapatkan, yang dicompare dengan parameter yang sudah didefinisikan). `Contoh: Alerting di Grafana`

4. Memiliki Keep-alive. Keep alive sama seperti Health-check, hanya saja keep-alive memiliki scope yang lebih sempit, yaitu untuk memastikan bahwa objek tersebut masih hidup (dapat di ping). Biasanya, objeknya terbatas hanya pada node.
5. Memiliki Heartbeat. Digunakan oleh node yang menjalankan layanan untuk saling memberikan paket komunikasi. Bertugas sebagai monitor dari node, network, network-interface, dan mencegah cluster partitioning / sharding. Hanya dapat direalisasikan oleh cluster yang menggunakan konsep Master-master (Masterless). `Contoh: Elasticsearch`

## **[Load-balancer]**

6. Memiliki load-balancer untuk distribusi traffic. Hanya dapat digunakan terhadap service-service yang menggunakan konsep Active-active. Load-balancer sangat erat kaitannya dengan health-check dan keep-alive untuk memastikan tidak ada trafik yang didistribusikan kepada node yang tidak sehat atau yang down.
